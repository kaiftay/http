import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.stream.Collectors;

public class HttpRequest {

    private HttpURLConnection connection;

    public HttpRequest(String url) throws IOException {
        this.connection = (HttpURLConnection) new URL(url).openConnection();
    }

    public HttpRequest setHttpMethod(String method) throws ProtocolException {
        connection.setRequestMethod(method);
        return this;
    }
    public HttpRequest addHeader(String key, String value) {
        connection.setRequestProperty(key, value);
        return this;
    }

    public String getResponse() throws IOException {
        return new BufferedReader(new InputStreamReader(connection.getInputStream())).lines()
                .collect(Collectors.joining("\n"));
    }

    public static void main(String[] args) throws IOException {
        HttpRequest httpRequest = new HttpRequest("https://httpbin.org/get");
        httpRequest.setHttpMethod("GET")
                .addHeader("Test-header", "test-key");
        System.out.println(httpRequest.getResponse());
    }
}